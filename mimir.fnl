#!/bin/env fennel

;; ";;#" is used for autogen of documentation


;;#* Snippet from aerc documentation
;;#=config-keyword= Specifies the command to be used to tab-complete email addresses.
;;#Any occurrence of =%s= in the address-book-cmd will be replaced
;;#with anything the user has typed after the last comma.
;;#
;;# The command must output the completions to standard output, one completion per line.
;;# Each line must be tab-delimited, with an email address occurring as the first field.
;;# Only the email address field is required.
;;# The second field, if present, will be treated as the contact name.
;;# Additional fields are ignored.
;;#
;;#
;;#* transpiled example on how to use
;;##+BEGIN_SRC fennel
;;#(local str "This is some text containing the word tiger.")
;;#(if (string.match str :tiger) (print "The word tiger was found.")
;;#    (print "The word tiger was not found."))
;;##+END_SRC
;;#
(local fennel (require :fennel))
(local configPath
	(..
	(or (os.getenv "XDG_CONFIG_DIR")
	(.. (os.getenv "HOME") "/.config"))
	"/fennelScripts"))
(table.insert (or package.loaders package.searchers) fennel.searcher)
(set package.path (.. package.path configPath))
(set fennel.path (.. fennel.path ";" configPath "/?.fnl"))
(local rc (require :mimirrc))
(local addr rc.addr)
(local settings (require :general))

(local mimir {})
(fn fmtOut [name mail matchon ...]
  "Function to format the input according to how aerc wants it.
more functions and options might come in the future.
"
  (print (table.concat [mail name matchon (or (table.concat [...] ",") "")] "\t"))
  )



(fn mimir.get [input]
  "Function to find any addr-entrys that matches the input, used for tab-completion in aerc"
  (local outTbl {})
  (each [key value (ipairs addr)]
	(if (string.find value.name input)
		(fmtOut value.name value.mail :name)
		(string.find value.mail input)
		(fmtOut value.name value.mail :mail)
		(each [k alias (ipairs value.alias)]
		  (if (string.find alias input)
			  (fmtOut value.name value.mail alias	:alias)
			  )
		  )
		)
	)
  )




(mimir.get
	(. arg (# arg))
	(. arg (- (# arg) 1))
)
