* description
a simple script made to work as the addressbook tab-complete for the aerc terminal mail client

a way to make it work with dmenu,rofi -dmenu, fzf and others
(anything that works like dmenu should work) is in the works

* function descriptions
currently there are only(?) 2 functions in this script
#+begin_src fennel
(fn fmtOut [name mail matchon ...]
"Function to format the input according to how aerc wants it.
more functions and options might come in the future.")
#+end_src


#+begin_src fennel
   (fn mimir.get [input]
	 "Function to find any addr-entrys that matches the input,
	  used for tab-completion in aerc"
   )
  #+end_src

